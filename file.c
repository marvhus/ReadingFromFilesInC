#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
    char* buffer = NULL;
    long length = 0;
    FILE* f = fopen("test.txt", "rb");

    if (f)
    {
        fseek(f, 0, SEEK_END);
        length = ftell(f);
        fseek(f, 0, SEEK_SET);
        buffer = malloc(length);
        if (buffer) fread(buffer, 1, length, f);
        fclose(f);
    } else {
        printf("Failed to read file\n");
        exit(1);
    }

    if (buffer)
    {
        printf("Length: %ld\nContent: %s\n", length, buffer);
    }
}
